package com.example.demo.controller;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerWithServiceIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Test
    void whenGetOne_thenReturnValidResponse() throws Exception {
        String testText = "Test";
        ToDoEntity testEntity = new ToDoEntity(1l, testText);
        when(toDoRepository.findById(testEntity.getId())).thenReturn(
                Optional.of(testEntity));
        this.mockMvc
                .perform(get("/todos/{id}", testEntity.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(testText))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenGetOneIdNotFound_thenReturnError() throws Exception {
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());
        this.mockMvc
                .perform(get("/todos/{id}", "101"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")))
                .andExpect(content().string("Can not find todo with id 101"));
    }

    @Test
    void whenComplete_thenReturnValidResponse() throws Exception {
        ToDoEntity testEntity = new ToDoEntity(1l, "Test");
        when(toDoRepository.findById(testEntity.getId())).thenReturn(
                Optional.of(testEntity));
        testEntity.completeNow();
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenReturn(testEntity);
        this.mockMvc
                .perform(put("/todos/{id}/complete", testEntity.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(testEntity.getText()))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(testEntity.getId()))
                .andExpect(jsonPath("$.completedAt").exists());
    }

    @Test
    void whenCompleteIdNotFound_thenReturnError() throws Exception {
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());
        this.mockMvc
                .perform(put("/todos/{id}/complete", 101))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")))
                .andExpect(content().string("Can not find todo with id 101"));
    }

    @Test
    void whenSave_thenReturnValidResponse(@Autowired MockMvc mockMvc) throws Exception {
        ToDoEntity testEntity = new ToDoEntity(1l, "Test");
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenReturn(testEntity);
        mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{  \"text\": \"" + testEntity.getText() + "\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(testEntity.getText()))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenSaveIdNotFound_thenReturnError() throws Exception {
        ToDoEntity testEntity = new ToDoEntity(101l, "Test");
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());
        this.mockMvc
                .perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{  \"id\":" + testEntity.getId() +
                                ", \"text\": \"" + testEntity.getText() + "\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")))
                .andExpect(content().string("Can not find todo with id 101"));
    }


}
