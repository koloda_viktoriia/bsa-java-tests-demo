package com.example.demo;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ActiveProfiles(profiles = "test")
@AutoConfigureMockMvc
public class ToDoAllLayersIT {
    @Test
    void save_thenReturnValidResponse(@Autowired MockMvc mockMvc) throws Exception {
        String testText = "My to do text";
        mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{  \"text\": \"" + testText + "\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(testText))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void complete_thenReturnValidResponse(@Autowired MockMvc mockMvc, @Autowired ToDoRepository repository) throws Exception {
        ToDoEntity test = repository.save(new ToDoEntity("Test"));
        mockMvc
                .perform(put("/todos/{id}/complete", test.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(test.getText()))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(test.getId()))
                .andExpect(jsonPath("$.completedAt").exists());
    }

}
