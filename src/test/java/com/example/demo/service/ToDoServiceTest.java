
package com.example.demo.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import static org.mockito.Mockito.*;
//import static org.mockito.AdditionalAnswers.*;
import org.mockito.ArgumentMatchers;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;

class ToDoServiceTest {

    private ToDoRepository toDoRepository;

    private ToDoService toDoService;

    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        toDoService = new ToDoService(toDoRepository);
    }

    @Test
    void whenGetAllEmpty_thenReturnListWithNullSize() {
        var test = toDoService.getAll();
        assertTrue(test.size()==0);

    }

    @Test
    void whenCompleteAlreadyHasCompleteAt_thenReturnWithCompletedAt() throws ToDoNotFoundException {
        var completeTime = ZonedDateTime.now(ZoneOffset.UTC);
        System.out.println(completeTime);
        ToDoEntity test = new ToDoEntity(0l, "Test 1", completeTime);
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(test));
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenReturn((test.completeNow()));
        var result = toDoService.completeToDo(test.getId());
        assertTrue(result.id == test.getId());
        assertTrue(result.text.equals(test.getText()));
        assertTrue(result.completedAt.isAfter(completeTime));
    }

    @Test
    void whenCompleteIdNotFound_thenThrowNotFoundException() {
        assertThrows(ToDoNotFoundException.class, () -> toDoService.completeToDo(1l));
    }

    @Test
    void whenUpserIdNotFound_thenThrowNotFoundException() {
        ToDoSaveRequest testToDo = new ToDoSaveRequest();
        testToDo.id = 1L;
        testToDo.text = "Test";
        assertThrows(ToDoNotFoundException.class, () -> toDoService.upsert(testToDo));
    }


}
